package zbuild

/**
  * Created by WG on 2016/05/17.
  */

import com.twitter.util.Await
import org.wgmouton.common.WebServiceInterface.Interpreters.Finagle._
import org.wgmouton.common.WebServiceInterface._

object WebService {
  def main(args: Array[String]): Unit = {

    val slack = WSClient(TCPProtocol.HTTP, "localhost", 9000)((host, ports) ⇒
      com.twitter.finagle.Http.newService(ports.map(host + ":" + _).mkString(","))
    )

    val response = WSCall(GET,  "http://google.com")
//
//    response.map { r ⇒
//      println(r.contentString)
//      r
//    }
//
    val wR = Await.result(response)

    println(wR.contentString)
    //println(org.wgmouton.BuildInfo.version)



  }
}
