//package org.wgmouton.common
//
//import java.net.URL
//
//import com.twitter.finagle._
//import com.twitter.finagle.http.{Method, RequestBuilder, RequestConfig, Response}
//import com.twitter.io.Buf
//import com.twitter.util.Future
//
//import scalaz.Scalaz
//import Scalaz._
//import language._
//
///**
//  * Created by WG on 2016/05/09.
//  */
//
//object Url {
//  def apply(uri: String): Url = new Url(uri)
//}
//
//class Url(uri: String) {
//
//  import java.net.URLEncoder
//
//  private def encode(s: String) = URLEncoder.encode(this.uri, "UTF-8")
//
//  def map(f: String ⇒ String): Url = Url(f(uri))
//
//  def flatMap(f: String ⇒ Url): Url = f(uri)
//
//  def withQueryString(t2: (String, String)*): Url = map(t2.foldLeft(_) {
//    case (url, (key, value)) ⇒ s"$url${url contains "?" fold("&", "?")}$key=${encode(value)}"
//  })
//
//  def url: String = uri
//}
//
//object FinagleClient {
//
//  import com.twitter.finagle.Service, Service._
//  import com.twitter.finagle.Http
//  import com.twitter.finagle.http.{Request, Response}
//
//  val egometBackend = Http.client.newService("somewhere")
//  val e = egometBackend(Request(Method.Post, "/", ""))
//  val f = Http.fetchUrl("hi")
//
//  def baseRequest(url: Url, f1: RequestBuilder[RequestConfig.Yes, Nothing] ⇒ Request) = {
//    val url = new URL("")
//    val addr = {
//      val port = if (url.getPort < 0) url.getDefaultPort else url.getPort
//      Address(url.getHost, port)
//    }
//    val request = http.RequestBuilder().url(url).buildGet()
//    val service = Http.client.newService(Name.bound(addr), "")
//    service(request) ensure {
//      service.close()
//    }
//
//    //    val r = ClientBuilder().[Request, Response]().(RequestBuilder().url(Url("hi").url).buildPost(Buf.ByteArray("dhwau".getBytes: _*)))
//  }
//
//  object BaseRequest {
//    hallo ⇒
//    def apply(url: Url, f1: RequestBuilder[RequestConfig.Yes, Nothing] ⇒ Request) = hallo.apply()
//
//    def apply(f1: RequestBuilder[RequestConfig.Yes, Nothing] ⇒ Request)(implicit url: Url) = ""
//  }
//
//  object GET {
//    def apply(url: Url) = baseRequest(url, _.buildGet())
//  }
//
//  object POST {
//    def apply(url: Url, data: String) = baseRequest(Url("hi"), _.buildPost(Buf.ByteArray("dw".getBytes: _*)))
//  }
//
//  //  val d = egometBackend.POST("", "")
//
////  implicit def finagleClient(service: Service[Request, Response]): BaseREST[Future[Response]] = new BaseREST[Future[Response]] {
////    override def POST(uri: String, data: String) = service(Request(Method.Post, "hi"))
////
////    Option
////  }
//}
//
//
////trait BaseRequest[+REQ, +RES] {
////  def get(): RES = ???
////
////
////  def post(): RES = ???
////}
//
////object BaseRequest extends BaseRequest[Nothing, Future[Response]] {
////  override def get()
////}
//
//object SprayClient {
//
//}
//
////trait BaseREST[A] {
////  def POST(uri: String, data: String): A = ???
////
////  def GET: Future[Response] = ???
////}
//
//
//object RestClient {
//
//}
