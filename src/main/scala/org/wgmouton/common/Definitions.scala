package org.wgmouton.common

import scalaz.\/

/**
  * Created by WG on 2016/04/18.
  */
object Definitions {

  def -\/-[A](a: ⇒ A): Throwable \/ A = \/.fromTryCatchNonFatal(a)

  def -\/-[A, B](a: Either[A, B]): A \/ B = \/.fromEither(a)
}
