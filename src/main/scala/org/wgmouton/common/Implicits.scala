package org.wgmouton.common

/**
  * Created by WG on 2016/04/15.
  */

import scalaz.\/
import scala.language.implicitConversions
import scala.concurrent.Future, concurrent.ExecutionContext.Implicits.global

object Implicits {

  /**
    * This trait is used with sequenceDisjunction definition to convert a list[\/[A, B]] to a \/[A, List[B]]
    *
    * @tparam A Any
    * @tparam B Any
    */
  sealed trait DoubleType[A, B] {
    def sequenceD: A \/ List[B]
  }

  implicit def sequenceDisjunction[A, B](l: List[A \/ B]): DoubleType[A, B] = new DoubleType[A, B] {
    /**
      * Convert a list[\/[A, B]] to a \/[A, List[B]]
      *
      * @return
      */
    override def sequenceD = l.foldRight(\/.right[A, List[B]](List[B]()))((l, pl) ⇒ pl.flatMap(pl ⇒ l.map(List(_) ::: pl)))
  }

  sealed trait SingleType[A] {
    def future: Future[A]
  }

  implicit def AToFuture[A](r: A): SingleType[A] = new SingleType[A] {
    override def future: Future[A] = Future(r)
  }
}
