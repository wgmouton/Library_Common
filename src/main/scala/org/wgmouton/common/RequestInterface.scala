//package org.wgmouton.common
//
///**
//  * Created by WG on 2016/05/11.
//  */
//
//import com.twitter.util.Future
//
//import scalaz.{-\/, Scalaz, \/}
//import Scalaz._
//import language._
//
//object BaseREST {
//
//  object Url {
//    implicit def apply(url: String): Url = new Url(url)
//  }
//
//  class Url(url: String) {
//
//    import java.net.URLEncoder
//
//    private def encode(s: String) = URLEncoder.encode(this.url, "UTF-8")
//
//    def map(f: String ⇒ String): Url = Url(f(url))
//
//    def flatMap(f: String ⇒ Url): Url = f(url)
//
//    def withQueryString(t2: (String, String)*): Url = map(t2.foldLeft(_) {
//      case (url, (key, value)) ⇒ s"$url${url contains "?" fold("&", "?")}$key=${encode(value)}"
//    })
//
//    def /(url: String): Url = map(_.concat("/" + url.trim))
//
//    def /?(t2: (String, String)*): Url = withQueryString(t2: _*)
//
//    def get: String = url
//  }
//
//  sealed trait Verb
//
//  case object GET extends Verb
//
//  case object POST extends Verb
//
//  object FinagleToConnection {
//
//    import com.twitter.finagle.{http, Http, Service}
//
//    implicit def buildRequest[A](server: Server[A], url: String) = server.map {
//      case service: Service[http.Request, http.Response] ⇒ service(http.RequestBuilder(""))
//    }
//
//    implicit def doRequest[A, C](verb: Verb, request: Request[A], f1: String ⇒ C): Future[Throwable \/ C] = request.map {
//      case service: Service[http.Request, http.Response] ⇒ service()
//      case _ ⇒ Future(-\/(new Exception("Invalid Request")))
//    }
//
//    Future(\/.fromTryCatchNonFatal(f1("")))
//  }
//
//  trait RequestBuilder[A] {
//    def map[B](f1: A ⇒ B): RequestBuilder[B]
//
//    def flatMap[B](f1: A ⇒ RequestBuilder[B]): RequestBuilder[B]
//  }
//
//  case class Request[A](r: A) extends RequestBuilder[A] {
//    override def map[B](f1: A ⇒ B): Request[B] = Request(f1(r))
//
//    override def flatMap[B](f1: A ⇒ RequestBuilder[B]): RequestBuilder[B] = f1(r)
//  }
//
//  case class Server[A](s: A) extends RequestBuilder[A] {
//    override def map[B](f1: A ⇒ B): Server[B] = Server(f1(s))
//
//    override def flatMap[B](f1: A ⇒ RequestBuilder[B]): RequestBuilder[B] = f1(s)
//
//    def url[B](url: Url)(implicit f1: (Server[A], String) ⇒ Request[B]) = Request(f1(Server(s), url.get))
//
//    def /[B](url: Url)(implicit f1: String ⇒ Request[B]) = this.url(url)
//  }
//
//  case class Response[A](r: A) extends RequestBuilder[A] {
//    override def map[B](f1: A ⇒ B): Response[B] = Response(f1(r))
//
//    override def flatMap[B](f1: A ⇒ RequestBuilder[B]): RequestBuilder[B] = f1(r)
//  }
//
//  def parseTo[A]: String ⇒ A = (s: String) ⇒ s.asInstanceOf[A]
//
//  object Request {
//
//    import com.twitter.util.Future
//
//    def apply[A, B](method: Verb, url: Url, parseResponse: String ⇒ B, timeout: Int = 15)(implicit f1: (Verb, Request[A], String ⇒ B) ⇒ Future[Throwable \/ B]): Future[Throwable \/ B] = f1(method, request, parseResponse)
//
//    def apply[A, B](method: Verb, server: Server[A], url: Url, parseResponse: String ⇒ B)(implicit f1: (Verb, Request[A], String ⇒ B) ⇒ Future[Throwable \/ B]): Future[Throwable \/ B] = f1(method, request, parseResponse)
//  }
//
//}