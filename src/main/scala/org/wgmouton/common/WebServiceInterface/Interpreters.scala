package org.wgmouton.common.WebServiceInterface

import com.twitter.logging.Logger
import HelperTypes._

/**
  * Created by WG on 2016/05/16.
  */
object Interpreters {

  import com.twitter.util.Future


  trait Interpreter {

  }

  object Finagle extends Interpreter {

    import com.twitter.io.Buf
    import com.twitter.finagle._, http.{Request, Response, RequestBuilder, RequestConfig}
    import com.twitter.util.{Return, Throw}

    implicit def runWSClient(service: Service[Request, Response], verb: Verb, uri: Uri, data: Option[WSData], timeout: Int): WSState[Future[Response]] =
      for {
        service ← WSState(service)

        requestBuilder ← WSState(http.RequestBuilder().url(uri))

        request ← WSState((rb: RequestBuilder[RequestConfig.Yes, Nothing]) ⇒ (verb, data) match {
          case (_, Some(d)) ⇒ rb.addHeader("Content-Type", d.contentType.toString).build(http.Method(verb.toString), Some(Buf.Utf8(d.content.toString)))
          case _ ⇒ rb.build(http.Method(verb.toString), None)
        })

        response ← WSState(service(request(requestBuilder)))


      } yield response ensure {
        response.transform {
          case Return(r) => Future(println(r.contentString))
          case t: Throw[_] => Future(println("Error"))
        }
      }
  }

  object Spray extends Interpreter {

  }

}