package org.wgmouton.common.WebServiceInterface

/**
  * Created by WG on 2016/05/16.
  */
object URI {

}

class URI() {

}

sealed trait Verb

case object GET extends Verb

case object POST extends Verb

case object PUT extends Verb

case object PATCH extends Verb

case object DELETE extends Verb

sealed abstract class ContentType(name: String) {
  override def toString: String = name
}

object ContentType {

  case object Json extends ContentType("application/json")

  case object Text extends ContentType("text/plain")

  case object Xml extends ContentType("text/xml")

  private case class Custom(name: String) extends ContentType(name)

  def apply(name: String): ContentType = name.toLowerCase match {
    case "json" => Json
    case "plain" => Text
    case "xml" => Xml
    case method => Custom(name)
  }
}

sealed abstract class TCPProtocol(port: Int) {
  def getPort: Int = port

  def getProtocol: String = toString.toLowerCase
}

object TCPProtocol {

  case object HTTP extends TCPProtocol(80)

  case object HTTPS extends TCPProtocol(443)

  case object FTP extends TCPProtocol(21)

  case object SSH extends TCPProtocol(22)

  private case class Custom(protocol: String, p: Int) extends TCPProtocol(p) {
    override def getProtocol: String = protocol.toLowerCase
  }

  def apply(name: String): TCPProtocol = name.toLowerCase match {
    case "http" => HTTP
    case "https" => HTTPS
    case "ftp" => FTP
    case "ssh" => SSH
    case _ ⇒ Custom(name, -1)
  }
}

