package org.wgmouton.common.WebServiceInterface

/**
  * Created by WG on 2016/05/16.
  */

object HelperTypes {
  type Host = String
  type Ports = List[Int]
  type Uri = String
}

import HelperTypes._

case class WSData(contentType: ContentType, content: String) {

  //    def convertToString(implicit f: (A, ContentType) => String) = ""
}

case class WSState[A](s: A) {
  def map[B](f: A => B): WSState[B] = flatMap(a => WSState(f(a)))

  def flatMap[B](f: A => WSState[B]): WSState[B] = f(s)

  def get: A = s
}


case class WSClient[A, B](preferredTCPProtocol: TCPProtocol, host: String, port: Int*)(f: (Host, Ports) => A)(implicit val doRequest: (A, Verb, Uri, Option[WSData], Int) => WSState[B]) {
  def /(uri: Uri) = this → uri

  def buildService: A = f(host, port.toList)
}

object WSCall {

  import java.net.URL
  import scalaz.Scalaz._

  import com.twitter.finagle.{Service, http}, http.{Request, Response}

  private def defaultService(uRL: URL): WSClient[Service[Request, Response], com.twitter.util.Future[Response]] = {
    val port: Int = Int match {
      case _ if uRL.getPort > 0 => uRL.getPort
      case _ if TCPProtocol(uRL.getProtocol).getPort > 0 => TCPProtocol(uRL.getProtocol).getPort
      case _ => 80
    }
    WSClient[Service[Request, Response], com.twitter.util.Future[Response]](TCPProtocol(uRL.getProtocol), uRL.getHost, port)((host, ports) =>
      com.twitter.finagle.Http.newService(ports.map(host + ":" + _).mkString(","))
    )(Interpreters.Finagle.runWSClient)
  }

  private def actionRequest[A, B](client: WSClient[A, B], verb: Verb, url: String, content: Option[WSData],
                                  headers: Option[Map[String, String]], cookies: Option[String]): B =
  //Do WS Client request
    client.doRequest(client.buildService, verb, s"${client.preferredTCPProtocol.toString}://${client.host}/$url", content, 0).get


  //    def apply[A, B](verb: Verb, t2: (WSClient[A, B], Uri), data: WSData, headers: Option[Map[String, String]] = None): B =
  //      actionRequest(t2._1, verb, t2._2, data.some, None, None)
  //
  //    def apply[A, B](verb: Verb, t2: (WSClient[A, B], Uri), headers: Option[Map[String, String]] = None): B =
  //      actionRequest(t2._1, verb, t2._2, None, None, None)
  //
  //
  //    def apply(verb: Verb, url: String, data: WSData, headers: Option[Map[String, String]] = None) = {
  //      val nUrl = new URL(url)
  //      actionRequest(defaultService(nUrl), verb, nUrl.getPath, data.some, None, None)
  //    }

  def apply(verb: Verb, url: String, headers: Option[Map[String, String]] = None) = {
    val nUrl = new URL(url)
    actionRequest(defaultService(nUrl), verb, nUrl.getPath, None, None, None)
  }
}
