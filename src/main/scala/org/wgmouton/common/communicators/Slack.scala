package org.wgmouton.common.communicators

/**
  * Created by WG on 2016/05/24.
  */

import org.wgmouton.common.WebServiceInterface._, Interpreters.Finagle._

object Slack {
  def apply(apiKey: String, uesrname: String): Slack = new Slack(apiKey, uesrname)
}

class Slack(incomingWebHootUrl: String, uesrname: String) {

  case class SlackPayload(text: String, channel: Option[String] = None, username: Option[String] = None, icon_emoji: Option[String] = None)

  def sendMessage(message: String): Unit = {
    print(message)
    //    WSCall(POST, incomingWebHootUrl, WSData(ContentType.Json, SlackPayload(message, None, None, None)))
  }
}

object IFTTTmaker {

}

//class IFTTTmaker(key: String, )