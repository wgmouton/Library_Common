package org.wgmouton.common

/**
  * Created by WG on 2016/05/16.
  */

//Replace all
object Monads {
  case class State[S, +A](run: S ⇒ (S, A)) {
    def map[B](f: A ⇒ B): State[S, B] = flatMap(a ⇒ State.unit(f(a)))
    def flatMap[B](f: A ⇒ State[S, B]): State[S, B] = State { s ⇒
      val (s1, a) = run(s)
      f(a).run(s1)
    }
  }

  object State {
    def unit[S, A](a: A): State[S, A] = State {s ⇒ (s, a)}
    def map2[S, A, B, C](sa: State[S, A], sb: State[S, B])(f: (A, B) ⇒ C): State[S, C] = sa.flatMap(a ⇒ sb.map(b ⇒ f(a,b)))
    def map3[S, A, B, C, D](sa: State[S, A], sb: State[S, B], sc: State[S,C])(f: (A, B, C) ⇒ D): State[S, D] = sa.flatMap(a ⇒ sb.flatMap(b ⇒ sc.map(c ⇒ f(a,b,c))))
    def sequence[S, A](ls: List[State[S, A]]): State[S, List[A]] = ls.foldRight(unit[S,List[A]](List[A]()))((f,acc) ⇒ map2(f, acc)(_ :: _))
    def get[S]: State[S,S] = State { s ⇒ (s,s) }
    def gets[S, A](f: S ⇒ A) = State { s: S ⇒ (s, f(s))}
    def put[S](s: S): State[S, Unit] = State { _ ⇒ (s, ())}
    def modify[S](f: S ⇒ S): State[S, Unit] = State { s ⇒ (f(s), ())}
    def state[S, A](a:A): State[S, A] = State { s ⇒ (s, a)}
  }
}
