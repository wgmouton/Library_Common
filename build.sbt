name := "common"

organization := "org.wgmouton"

scalaVersion := "2.11.7"

lazy val `library_common` = (project in file(".")).enablePlugins(BuildInfoPlugin)

buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion)
buildInfoPackage := "org.wgmouton.common"
buildInfoUsePackageAsPath := true
buildInfoOptions += BuildInfoOption.ToJson

resolvers ++= Seq(
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
)

libraryDependencies ++= {
  Seq(
    "org.scalaz" %% "scalaz-core" % "7.2.2",
    "org.scalaz" %% "scalaz-effect" % "7.2.2",
    "org.scalaz" %% "scalaz-concurrent" % "7.2.2",
    "com.twitter" %% "finagle-http" % "6.35.0"
  )
}
//sourceGenerators in Compile += Def.task {
//  val file =
//  IO.write(file, """object Test extends App { println("Hi") }""")
//  Seq(file)
//}.taskValue

//unmanagedSourceDirectories in Compile += (sourceManaged in Compile).value  / "sbt-buildinfo" / "BuildInfo.scala"

// Publish to nexus
lazy val repository_realm = "Bintray API Realm"
lazy val repository_host = "api.bintray.com"
lazy val repository_username = sys.env.getOrElse("REPOSITORY_USERNAME", "")
lazy val repository_password = sys.env.getOrElse("REPOSITORY_PASSWORD", "")

//publishTo := Some("releases" at "https://api.bintray.com/maven/wgmouton/common/common/;publish=1")
//publishTo := Some("snapshots" at "https://api.bintray.com/maven/wgmouton/common/common/;publish=1")
//
//publishTo := {
//  if (version.value.trim.endsWith("SNAPSHOT"))
//    Some("snapshots" at nexus + "content/repositories/snapshots")
//  else
//    Some("releases" at nexus + "content/repositories/releases")
//}

credentials += Credentials(repository_realm, repository_host, repository_username, repository_password)

publishArtifact in Test := false


//managedSourceDirectories in Compile += baseDirectory.value / "target" / "scala-2.10" / "classes" / "org" / "wgmouton" / "common"


//val postBuildToSlack = taskKey[Unit]("Post build info to slack")
////val ver = hello.BuildInfo.version
//postBuildToSlack := {
//  println((baseDirectory.value.getParentFile / "src").getAbsolutePath)
//
//  WebService.main(Array())
////  for {
////    apiKey ← Option("")
////    username ← Option("")
////  } yield  _root_.classes.org.wgmouton.common.communicators.Slack(apiKey, username)
////    .sendMessage(s"${name.value}")
////  println("")
//}

//excludeFilter in unmanagedSources := "main.scala"

//lazy val postBuildToSlack = taskKey[Unit]("Generate my file")
//postBuildToSlack := (runMain in Compile).toTask(" WebService").value