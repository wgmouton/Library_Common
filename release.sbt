import ReleaseTransformations._, sbtrelease.{Version, versionFormatError, Vcs}

val processLogger: ProcessLogger = new ProcessLogger {
  def info(s: => String): Unit = println(s"[info] $s")

  def error(s: => String): Unit = println(s"[error] $s")

  def buffer[T](f: => T): T = f
}

def vcs(st: State): Vcs = Project.extract(st).get(releaseVcs).getOrElse(
  sys.error("Aborting release. Working directory is not a repository of a recognized VCS.")
)

def cmd(executable: String, baseDir: File)(args: Any*) = {
  lazy val exec = executableName(executable)

  def executableName(command: String) = {
    val maybeOsName = sys.props.get("os.name").map(_.toLowerCase)
    val maybeIsWindows = maybeOsName.filter(_.contains("windows"))
    maybeIsWindows.map(_ => command + ".exe").getOrElse(command)
  }

  sbt.Process(exec + args.mkString(" ", " ", ""), baseDir) ! processLogger
}

def git(args: String*) = { st: State ⇒
  cmd("git", vcs(st).baseDir)(args: _ *)
}

releaseVersion := (Version(_).map(_ → sys.env.getOrElse("REPO_BRANCH", "") match {
  case (v, "master") ⇒ v.copy(subversions = v.subversions.init :+ 0, qualifier = None).bumpMinor.string
  case (v, "develop") ⇒ v.bumpBugfix.copy(qualifier = Some("-rc")).string
  case (v, _) ⇒ v.string
}).getOrElse(versionFormatError))

lazy val updateVersion: ReleaseStep = { st: State =>
  import sbtrelease.ReleasePlugin.autoImport.ReleaseKeys._
  val extracted = Project.extract(st)
  val currentV = extracted.get(version)
  val releaseFunc = extracted.get(releaseVersion)
  val suggestedReleaseV = releaseFunc(currentV)
  val nextFunc = extracted.get(releaseNextVersion)
  val suggestedNextV = nextFunc(suggestedReleaseV)
  st.put(versions, (suggestedReleaseV, suggestedNextV))
}

def pushVersion(): ReleaseStep = { st: State ⇒
  (for {
    remote ← sys.env.get("REPO_REMOTE")
    branch ← sys.env.get("REPO_BRANCH")
  } yield {
    git(s"remote add release $remote")(st)
    git(s"push --follow-tags release ${vcs(st).currentHash}:$branch")(st)
  }).orElse(sys.error("Error pushing the version"))
  st
}

lazy val setWhoAmI: ReleaseStep = { st: State ⇒
  for {
    name ← sys.env.get("REPO_USER_NAME")
    email ← sys.env.get("REPO_USER_EMAIL")
  } yield {
    git(s"""config user.email "$email"""")(st)
    git(s"""config user.name "$name"""")(st)
  }
  st
}

//Commit message
releaseCommitMessage := s"Ci Setting version to ${(version in ThisBuild).value} [ci skip]"

//Tag name
releaseTagName := (version in ThisBuild).value

releaseProcess := Seq[ReleaseStep](
  setWhoAmI,
  checkSnapshotDependencies,
  updateVersion,
  setReleaseVersion,
  commitReleaseVersion,
    tagRelease,
  //releaseStepComm and("publish"),
    pushVersion()
)